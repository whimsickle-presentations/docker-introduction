FROM python:3.9.12-slim-bullseye
COPY ./app /app
CMD python3 /app/app.py
